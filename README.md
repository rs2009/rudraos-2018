# rudraOS
### An x86 operating system built from scratch in Assembly
This is an x86 operating system built from scratch in Assembly. Licensed by Rudra Saraswat of BlueFire, Inc.
This OS has a shell named tash. The OS has 1 command and one executable.

## Minimum Build Requirements
###### RAM - 1 GB (Recommended - 1 GB or more)
###### Hard Disk Space - 5 GBs (Estimated)
###### Processor - Any x86 or x64 based processor
###### Operating - Debian, Ubuntu (or any other Ubuntu-based distro)

## New features
###### Tash is colourful.
###### Has an about program that has specifications of the operating system.